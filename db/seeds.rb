10.times do
  FreelanceDocument.create!(
    title: Faker::Book.title,
    description: Faker::Lorem.sentence(3),
    file_url: 'http://cachefly.cachefly.net/100mb.test',
    image_url: 'https://i.imgur.com/7y2QGiO.jpg'
  )
end